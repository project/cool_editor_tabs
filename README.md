# CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## INTRODUCTION

 - This module improves the appearance of the Drupal admin tabs
   (view, edit, translate, .. links).
 - It shows a "settings" icon at a fixed position on the bottom right
   of the screen.
 - When clicking this icon, the admin tab items (create, edit, ..) pop up
   as clickable icons.
 - This is achieved in pure css without any javascript.
 - The css will only load for authenticated users, so anonymous users are
   uninfected if this module is enabled.
 - All icons are SVG icons and are minified into a single svg sprite, so the
   number of resources for this module is very low.

## REQUIREMENTS

This module has no other requirements outside of Drupal core.

## INSTALLATION

Install the cool_editor_tabs module as you would normally install a
contributed Drupal

 - require the repository:

      ```composer require drupal/cool_editor_tabs --prefer-dist```


 - enable the module:

      ```drush en cool_editor_tabs -y```


## CONFIGURATION

There is no configuration needed for this module. Just enable and
enjoy how good your admin tabs look now.

## MAINTAINERS

The 1.x branch was created by:
 - Dennis Cohn (dennis-cohn) - (https://www.drupal.org/u/dennis-cohn)

This module is basicly an fork of better_admin_tabs (https://www.drupal.org/project/better_admin_tabs) which is not longer being maintained.

All credits must go to Joery Lemmens (flyke) - (https://www.drupal.org/u/flyke)
