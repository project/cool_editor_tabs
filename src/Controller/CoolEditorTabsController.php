<?php

namespace Drupal\cool_editor_tabs\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class CoolEditorTabsController extends ControllerBase {
  public function content() {
    $user = \Drupal::currentUser();
    if (!$user->hasPermission('use cool editor tabs')) {
      return new Response('Access Denied', 403);
    }
    return [
      '#markup' => '<p>Welcome to Cool Editor Tabs!</p>',
    ];
  }
}
