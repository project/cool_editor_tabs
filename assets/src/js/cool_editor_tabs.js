/**
 * @file
 * Javascript functionality for the Cool Editor Tabs.
 */
(function (Drupal, once) {
  'use strict';
  Drupal.behaviors.coolEditorTabs = {
    attach: function (context) {
      let toggle_elements = once('coolEditorTabsExpand', '#js-admin-tabs-toggle', context);
      for (let i = 0; i < toggle_elements.length; i++) {
        let toggle_element = toggle_elements[i];
        toggle_element.addEventListener('click', function () {
          let tabs_list = toggle_element.closest('.admin-tabs').querySelector('#admin-tabs__list');
          tabs_list.setAttribute('aria-expanded', tabs_list.getAttribute('aria-expanded') === 'false' ? 'true' : 'false');
        });
      }
    },
  };
})(Drupal, once);
